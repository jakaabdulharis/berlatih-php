<?php
function tentukan_nilai($number)
{   
    $result = "";
    if($number >= 85 && $number <= 100){
        $result = "Sangat Baik <br>";
    }elseif($number >= 70 && $number <= 85){
        $result = "Baik <br>";
    }elseif($number >= 60 && $number <= 70){
        $result = "Cukup <br>";
    }else{
        $result = "Kurang <br>";
    }
    return $result;
}

//lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik”
//lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik”
//lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup”
//selain itu maka akan mereturn string “Kurang”

//TEST CASES
echo "<b>TEST CASE </b> <br>";
echo "---------------------- <br>";
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>